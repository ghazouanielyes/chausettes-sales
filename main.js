function processNumber(num) {
	let output = "";

	if (num % 3 === 0) {
		output += "Chausettes";
	}

	if (num % 5 === 0) {
		output += "Sales";
	}

	if (output === "") {
		return num.toString();
	}

	return output;
}


function print(){
	for(let i = 1; i <= 100; i++){
		console.log(processNumber(i));
	}
}

module.exports = {print, processNumber};

print();
