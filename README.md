# ChausettesSales Project

This project provides a solution for the "ChausettesSales" problem, where you print "Chausettes" for multiples of 3, "Sales" for multiples of 5, and "ChausettesSales" for multiples of both 3 and 5.

## Description

In this project, we have implemented a JavaScript function that takes a number as input and returns the appropriate output based on the conditions mentioned above. We also have unit tests to ensure the correctness of the function.

## How to Run the Tests

To run the tests and verify that the code works as expected, follow these steps:

1. **Install Node.js:** Make sure you have Node.js installed on your computer. You can download it from [https://nodejs.org/](https://nodejs.org/).

2. **Clone the Repository:** Clone this Git repository to your local machine using the following command:

   ```bash
   git clone https://gitlab.com/ghazouanielyes/chausettes-sales.git

3. **Navigate to the Project Directory:** Change your current directory to the project folder:

   ```bash
   cd chausettes-sales

4. **Install Dependencies:** Install the project dependencies by running:

   ```bash
   npm install

5. **Run the Tests:** Once the dependencies are installed, you can run the tests using the 

   ```bash
   npm test

Jest, the testing framework, will execute the tests, and you will see the test results displayed in your terminal.