const { processNumber } = require("./main");

test('Returns "Chausettes" for multiples of 3', () => {
	expect(processNumber(3)).toBe("Chausettes");
	expect(processNumber(9)).toBe("Chausettes");
	expect(processNumber(33)).toBe("Chausettes");
});

test('Returns "Sales" for multiples of 5', () => {
	expect(processNumber(5)).toBe("Sales");
	expect(processNumber(25)).toBe("Sales");
	expect(processNumber(100)).toBe("Sales");
});

test('Returns "ChausettesSales" for multiples of both 3 and 5', () => {
	expect(processNumber(15)).toBe("ChausettesSales");
	expect(processNumber(30)).toBe("ChausettesSales");
	expect(processNumber(45)).toBe("ChausettesSales");
});

test("Returns the number for non-multiples of 3 and 5", () => {
	expect(processNumber(1)).toBe("1");
	expect(processNumber(7)).toBe("7");
	expect(processNumber(98)).toBe("98");
});
